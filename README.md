# Super Projet
Product Owner:

Le Product Owner est le représentant des clients et des utilisateurs. Son objectif est de maximiser la valeur du produit développé. Il explicite les éléments (items) du carnet du produit. Il définit l'ordre dans lequel les fonctionnalités seront développées. Il prend les décisions importantes concernant le projet. Il s'assure que le carnet du produit est visible et compris de tous. Cela permet qu'à tout instant, chacun sache sur quoi travailler. Enfin, il valide fonctionnellement les développements.

![img](http://s1.edi-static.fr/Img/FICHEOUTIL/2016/9/308266/T_275072.jpg)



La Dev Team:

Les développeurs de l'équipe réalisent la partie "done" du Kanban. 

Les membres de l'équipe sont polyvalents. Il n'y a pas de hiérachie au sein du groupe. Les coéquipiers s'auto organisent. 


La DevTeam est généralement composée de 3 à 10 personnes. 